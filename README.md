----- BASE ON U-BOOT 2019-07 ----- 
---


**BUILD U-BOOT** 

*make sure your ubuntu have gcc 6 or higher.* 

1\. Open your terminal and run "**export CROSS_COMPILE=arm-linux-gnueabi-**" to export gcc library. 

2\. To choose your target, you can see them in /config directory. Then run "**make *_defconfig**" to create .config  

(**make turris_omnia_defconfig** for Turris Omnia and **make clearfog_defconfig** for Clearfog) 

3\. If you want to build uboot supporting UART, run **make menuconfig**. Then choose **ARM architecture -> Boot method -> UART**. 

4\. Finally, run **make** to build your uboot.  

**FLASH U-BOOT INTO RAM**

1\. Connect ethernet cable into your router

2\. In folder include sendbeacon and kwboot,If your router is *Turris Omnia*, run "**sudo ./sendbeacon /dev/ttyUSB0**" in source folder.

3\. Then run "**sudo ./kwboot -t -B 115200 /dev/ttyUSB0 -b u-boot-spl.kwb**" to download U-boot image into router

**FLASH NEW U-BOOT**

1\. When flash U-boot into RAM finally done. In U-boot interface, run "**dhcp**" to setup IP address

2\. Clean eMMC and download new uboot image into router

**mw 0x1000000 0 0x100000**

**tftpboot 0x1000000 u-boot-spl.kwb**

3.\ Write imgae to eMMC

**sf probe**

**sf erase 0x0 0x100000**

**sf write 0x1000000 0x0 0x100000**

---
----------------------------------- **USAMI** -----------------------------------
